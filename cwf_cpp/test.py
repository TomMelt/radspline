from cwf_wrapper import cwf
from scipy.special import spherical_jn as jl
from scipy.special import spherical_yn as nl
import matplotlib.pyplot as plt
import numpy as np

print(cwf.__doc__)

def cwf_wrapper(z, l, eta):
    zr, zi = z.real, z.imag
    lr, li = l.real, l.imag
    etar, etai = eta.real, eta.imag
    temp = cwf(zr, zi, lr, li, etar, etai)

    f = temp[0] + 1.j*temp[1]
    g = temp[2] + 1.j*temp[3]
    df = temp[4] + 1.j*temp[5]
    dg = temp[6] + 1.j*temp[7]

    return f, g, df, dg

z = np.linspace(1e-2,20,2000)
z = z + 0.3j*z
lmax = 5
eta = 1. + 0.5j

fig, axes = plt.subplots(4,1)

for l in range(lmax+1):

    f = np.zeros(len(z), dtype=np.complex)
    g = np.zeros(len(z), dtype=np.complex)
    df = np.zeros(len(z), dtype=np.complex)
    dg = np.zeros(len(z), dtype=np.complex)

    ys = {
        'f': f,
        'g': g,
        'df': df,
        'dg': dg,
    }

    for i, x in enumerate(z):
        f[i], g[i], df[i], dg[i] = cwf_wrapper(z=x, eta=eta, l=l)

    for j, funcs in enumerate(['f', 'g', 'df', 'dg']):
        axes[j].plot(z.real, ys[funcs].real, '-', color='C'+str(l), label='l='+str(l))
        axes[j].set_ylim(-10,10)
        axes[j].set_xlim(0,20)

axes[0].set_title(f'eta = {eta}, (real axis for imag z)')
plt.legend()
plt.show()

z = np.linspace(1e-2,20,2000)
lmax = 5
eta = 1.

fig, axes = plt.subplots(4,1)

for l in range(lmax+1):

    f = np.zeros(len(z), dtype=np.complex)
    g = np.zeros(len(z), dtype=np.complex)
    df = np.zeros(len(z), dtype=np.complex)
    dg = np.zeros(len(z), dtype=np.complex)

    ys = {
        'f': f,
        'g': g,
        'df': df,
        'dg': dg,
    }

    for i, x in enumerate(z):
        f[i], g[i], df[i], dg[i] = cwf_wrapper(z=x, eta=eta, l=l)

    for j, funcs in enumerate(['f', 'g', 'df', 'dg']):
        axes[j].plot(z.real, ys[funcs].real, '-', color='C'+str(l), label='l='+str(l))
        axes[j].set_ylim(-2,2)
        axes[j].set_xlim(0,20)

axes[0].set_title(f'eta = {eta}, (real only - bessel)')
plt.legend()
plt.show()
