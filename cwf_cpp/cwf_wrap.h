#include <vector>

std::vector<double> coulomb_functions(double zr, double zi, double lr, double li, double etar, double etai);

std::vector<double> hankel_functions(int pm, double zr, double zi, double lr, double li, double etar, double etai);
