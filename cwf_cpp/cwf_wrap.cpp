#include <iostream>
#include <fstream>
#include <string>
#include <complex>
#include <vector>

using namespace std;

#define SIGN(a) (((a) < 0) ? (-1) : (1))

const double precision = 1E-10,sqrt_precision = 1E-5;

#include "complex_functions.H"
#include "cwfcomp.cpp"

vector<double> coulomb_functions(double zr, double zi, double lr, double li, double etar, double etai){

  vector<double> f;

  complex<double> l = complex<double>(lr, li);
  complex<double> eta = complex<double>(etar, etai);
  complex<double> z = complex<double>(zr, zi);

  class Coulomb_wave_functions cwf(true,l,eta);

  complex<double> F,dF,G,dG;

  cwf.F_dF (z,F,dF);
  cwf.G_dG (z,G,dG);

  f.push_back(F.real());
  f.push_back(F.imag());

  f.push_back(G.real());
  f.push_back(G.imag());

  f.push_back(dF.real());
  f.push_back(dF.imag());

  f.push_back(dG.real());
  f.push_back(dG.imag());
  return f;
}

vector<double> hankel_functions(int pm, double zr, double zi, double lr, double li, double etar, double etai){

  vector<double> f;

  complex<double> l = complex<double>(lr, li);
  complex<double> eta = complex<double>(etar, etai);
  complex<double> z = complex<double>(zr, zi);

  //Use Un-normalized function to stop overflow errors
  //For Siegert states we solve (F-R a dF)=0 and the normalization doesn't matter
  class Coulomb_wave_functions cwf(false,l,eta);

  complex<double> H,dH;

  cwf.H_dH(pm, z, H, dH);

  f.push_back(H.real());
  f.push_back(H.imag());

  f.push_back(dH.real());
  f.push_back(dH.imag());

  return f;
}
