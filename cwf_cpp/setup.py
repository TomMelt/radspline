from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
        Extension(
            "cwf_wrapper",
            ["wrapper.pyx", "cwf_wrap.cpp"],
            language='c++',
            extra_compile_args=['-w'],
            )
        ]

setup(
        cmdclass={'build_ext': build_ext},
        ext_modules=ext_modules)
