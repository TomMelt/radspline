from libcpp.vector cimport vector

#----------------- COULOMB FUNCTIONS -----------------------------------
cdef extern from "cwf_wrap.h":
    vector[double] coulomb_functions(double zr, double zi, double lr, double li, double etar, double etai)

def cwf_coulomb(double zr, double zi, double lr, double li, double etar, double etai):
    """
Python wrapper for cwfcomp.cpp
Computes the Coulomb functions and their first derivatives
for imaginary z, eta and l.

Parameters
----------
zr : input float
zi : input float
lr : input int
li : input int
etar : input float
etai : input float

Returns
-------
Fr : float
Fi : float
Gr : float
Gi : float
dFr : float
dFi : float
dGr : float
dGi : float
    """
    return coulomb_functions(zr, zi, lr, li, etar, etai)

#----------------- HANKEL FUNCTIONS -----------------------------------
cdef extern from "cwf_wrap.h":
    vector[double] hankel_functions(int pm, double zr, double zi, double lr, double li, double etar, double etai)

def cwf_hankel(int pm, double zr, double zi, double lr, double li, double etar, double etai):
    """
Python wrapper for cwfcomp.cpp
Computes the Hankel functions and their first derivatives
for imaginary z, eta and l.

Parameters
----------
pm : input int (+1 for H+ / -1 for H-)
zr : input float
zi : input float
lr : input int
li : input int
etar : input float
etai : input float

Returns
-------
Hr : float
Hi : float
dHr : float
dHi : float
    """
    return hankel_functions(pm, zr, zi, lr, li, etar, etai)
