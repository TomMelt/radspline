from setuptools import setup

setup(
        name="radspline",
        author='Thomas Meltzer',
        author_email='thomas.meltzer1@gmail.com',
        url='https://gitlab.com/TomMelt/radspline',
        description="Solve 1D radial Schrodinger equation using R-matrix.",
        version='0.0.1',
        license='MIT',
        packages=['radspline'],
        install_requires=[
            'Cython',
            'geomdl',
            'scipy',
            'f90nml',
            'numpy',
            'pytest-allclose',
            ],
        )
