from geomdl.helpers import basis_function_ders_one as basis
import numpy as np


class BsplineBasisSet():
    """Generate a Bspline basis set"""
    def __init__(self, rmin, rmax, segments, degree, grid_type):

        self.rmin = rmin
        self.rmax = rmax
        self.segments = segments
        self.degree = degree
        self.grid_type = grid_type

        knots = self._initialise_knots()
        max_num = len(knots) - degree - 1

        self.knots = knots
        self.max_num = max_num

        # from 1, max_num because the 0th Bspline does not obey
        # the boundary condition at r=0.0
        self.btos = [BsplineBasisFunc(degree, knots, n) for n in range(1, max_num)]

    def _initialise_knots(self):
        rmin = self.rmin
        rmax = self.rmax
        segments = self.segments
        grid_type = self.grid_type
        degree = self.degree

        knots = []

        for i in range(segments):
            if grid_type.lower() == 'linear':
                r = rmin + (rmax - rmin) / float(segments - 1) * float(i)
                knots.append(r)
            elif grid_type.lower() == 'exponential':
                gamma = 5.
                r = (rmax - rmin) * (np.exp(gamma * (float(i) / float(segments - 1))) - 1.)
                r = r / (np.exp(gamma) - 1.)
                r = rmin + r
                knots.append(r)
            else:
                msg = f'{grid_type} is not supported.'
                msg += '\nPlease use [linear] or [exponential].'
                raise ValueError(msg)

        knots = degree * [knots[0]] + knots + degree * [knots[-1]]

        return np.array(knots)


class BsplineBasisFunc():
    """docstring for BsplineBasisFunc"""
    def __init__(self, degree, knots, num):
        self.degree, self.knots, self.num = degree, knots, num

        self.max_num = len(knots) - degree - 1
        self.eval = np.vectorize(self._eval, signature='()->(3)')
        self.support = self._support()
        self.segments = self._segments()
        self.num = num

    def _support(self):
        degree, knots, num = self.degree, self.knots, self.num
        rmin = knots[num]
        rmax = knots[num + degree + 1]
        return (rmin, rmax)

    def _segments(self):
        degree, knots, num = self.degree, self.knots, self.num
        segments = knots[num:num + degree + 2]
        segments = list(zip(segments[:-1], segments[1:]))
        temp = []
        for a, b in segments:
            if a < b:
                temp.append((a, b))
        segments = temp
        return segments

    def _eval(self, r):
        degree, knots, num = self.degree, self.knots, self.num
        if num >= self.max_num - 2 and np.isclose(r, max(knots)):
            # make r slightly smaller so we can get the value of the b-splines
            # on the end knot. Only required for the last two b-splines.
            r -= 1e-8

        # if order = 2 you get the 0th, 1st and 2nd derivatives.
        # knot is position you wish to evaluate B-spline at.
        derivs = basis(degree=degree, knot_vector=knots, span=num, knot=r, order=2)

        derivs = np.array(derivs)

        return derivs


def locate_knot(r, knots):
    """
    locate_knot finds the index of the knot which is closest to the value r.
    """

    dr = 1e-8
    temp = np.abs(knots - r)
    idx = temp.argmin()

    if temp.min() > dr:
        msg = f'There is no knot close to the value r={r}.'
        msg += f'\nNearest value is {knots[idx]}'
        raise ValueError(msg)

    return idx
