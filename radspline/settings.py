import f90nml
from dataclasses import dataclass, field, fields


@dataclass
class PotentialOpts:
    charge: float = 0.
    width: float = 0.
    strength: float = 0.
    model: str = ''

    def __post_init__(self):
        for field in fields(self):
            value = getattr(self, field.name)
            if not isinstance(value, field.type):
                msg = f'Namelist variable [{field.name}] must be of type {field.type}.'
                msg += f'\nHowever {type(value)}[{field.name} = {value}] was received.'
                raise ValueError(msg)


@dataclass
class BsplineOpts:
    rmax: float
    segments: int
    degree: int
    rmin: float = 0.
    grid_type: str = 'linear'

    def __post_init__(self):
        for field in fields(self):
            value = getattr(self, field.name)
            if not isinstance(value, field.type):
                msg = f'Namelist variable [{field.name}] must be of type {field.type}.'
                msg += f'\nHowever {type(value)}[{field.name} = {value}] was received.'
                raise ValueError(msg)


@dataclass
class CalculationOpts:
    name: str
    k_grid: list = field(default_factory=list)
    diff_grid_theta: list = field(default_factory=list)
    lmax: int = 0
    save_inner: bool = True
    save_outer: bool = True
    debug: int = 0

    def __post_init__(self):
        if self.name == '':
            msg = 'namelist [name] is empty.'
            msg += '\nPlease specify run name in the namelist.'
            raise ValueError(msg)
        for field in fields(self):
            value = getattr(self, field.name)
            if not isinstance(value, field.type):
                msg = f'Namelist variable [{field.name}] must be of type {field.type}.'
                msg += f'\nHowever {type(value)}[{field.name} = {value}] was received.'
                raise ValueError(msg)


class Options:
    class __Options:
        def __init__(self, config_path):
            self.config_path = config_path
            # read your config/vars file here
            # it will only be run once unless config_path is changed
            # in that case it will be reloaded
            namelist = f90nml.read(config_path)

            with open(self.config_path, 'r') as infile:
                self.raw_nml = infile.read()

            for key, value in namelist.items():
                key = key.lower()
                if key == 'potential':
                    self.potential = PotentialOpts(**value)
                elif key == 'bspline':
                    self.bspline = BsplineOpts(**value)
                elif key == 'calculation':
                    self.calculation = CalculationOpts(**value)
                else:
                    msg = f'[{key}] is not a supported namelist.'
                    raise KeyError(msg)

    instance = None
    config_path = None

    def __init__(self, config_path=None):
        if not Options.instance or (
                config_path != Options.config_path and config_path != None
                ):
            # only create real instance when needed (once)
            # save it on the Class (static)
            Options.instance = Options.__Options(config_path)

    def __getattr__(self, key):
        if not hasattr(Options.instance, key):
            path = Options.instance.config_path
            msg = f'Options key [{key}] missing from {path}'
            raise KeyError(msg)

        # change behaviour to use the single instance's fields/attributes
        return getattr(Options.instance, key)

    def __repr__(self):
        attrs = ['potential', 'bspline', 'calculation']
        string = [getattr(self, attr).__repr__() for attr in attrs]
        return '\n'.join(string)

    def __str__(self):
        attrs = ['potential', 'bspline', 'calculation']
        string = [getattr(self, attr).__str__() for attr in attrs]
        return '\n'.join(string)
