from scipy.special import spherical_jn as jl
from scipy.special import spherical_yn as nl
import numpy as np


@np.vectorize
def matching_condition(k, l, V0, r, mu=1.0):
    """obtain matching condition for a square well potential of strength V0
    e.g. S(Kr) = A*S(kr)+B*C(kr)     (match amplitude)
         S'(Kr) = A*S'(kr)+B*C'(kr)  (match first derivative)

    Keyword Arguments
    :k: float
        momentum of scattering particle
    :l: int
        angular momentum of scattering particle
    :V0: float
        strength of the potential
    :r: float
        match solutions at r=r. For a square potential this is the width of the
        potential e.g., r=width

    :returns: tuple(float, float)
        A and B are return as a tuple.

    """

    K = np.sqrt(2. * mu * V0 + k * k)
    e = K * r * jl(l, K * r)
    f = K * (jl(l, K * r) + K * r * jl(l, K * r, True))

    a = k * r * jl(l, k * r)
    b = k * r * nl(l, k * r)
    c = k * (jl(l, k * r) + k * r * jl(l, k * r, True))
    d = k * (nl(l, k * r) + k * r * nl(l, k * r, True))

    A = (e * d - f * b) / (a * d - c * b)
    B = (f - A * c) / d

    return A, B


@np.vectorize
def phaseshift(k, A, B):
    d = np.arctan2(-B, A)
    return d
