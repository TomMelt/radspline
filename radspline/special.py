#from .coulcc import coulcc_wrapper
#from .coul90 import coul90_wrapper
from .cwf_wrapper import cwf_coulomb, cwf_hankel
import numpy as np


#@np.vectorize
#def coulomb_functions(x, eta, l, funcs='all'):
#    f, g, df, dg, ifail = coulcc_wrapper(x=x, eta=eta, nl=l)
#    if ifail == 0:
#        funcs = funcs.lower()
#        if funcs == 'all':
#            return f, g, df, dg
#        elif funcs == 'f':
#            return f
#        elif funcs == 'g':
#            return g
#        elif funcs == 'df':
#            return df
#        elif funcs == 'dg':
#            return dg
#    else:
#        msg = f'COULCC :: ifail = {ifail}'
#        raise ValueError(msg)


#@np.vectorize
#def f(x, l, eta):
#    xr, xi = x.real, x.imag
#    lr, li = l.real, l.imag
#    etar, etai = eta.real, eta.imag
#    temp = cwf(xr, xi, lr, li, etar, etai)
#
#    f = temp[0] + 1.j*temp[1]
#    g = temp[2] + 1.j*temp[3]
#    df = temp[4] + 1.j*temp[5]
#    dg = temp[6] + 1.j*temp[7]
#
#    return f

def coulomb_functions(x, l, eta):
    xr, xi = x.real, x.imag
    lr, li = l.real, l.imag
    etar, etai = eta.real, eta.imag
    temp = cwf_coulomb(xr, xi, lr, li, etar, etai)

    f = temp[0] + 1.j*temp[1]
    g = temp[2] + 1.j*temp[3]
    df = temp[4] + 1.j*temp[5]
    dg = temp[6] + 1.j*temp[7]

    return f, g, df, dg


def hankel_functions(pm, x, l, eta):
    # Ricatti-Hankel functions (asymptotic behaviour is exp(+ikr) for l=0)
    pm = int(pm)
    xr, xi = x.real, x.imag
    lr, li = l.real, l.imag
    etar, etai = eta.real, eta.imag
    temp = cwf_hankel(pm, xr, xi, lr, li, etar, etai)
    H = temp[0] + 1.j*temp[1]
    dH = temp[2] + 1.j*temp[3]
    return H, dH
