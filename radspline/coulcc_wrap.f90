subroutine coulcc_wrapper(x,eta,nl, f,g,df,dg, ifail)
      implicit none

      integer, parameter :: dp = selected_real_kind(15)
      integer, intent(out) :: ifail
      integer, intent(in) :: nl
      complex(dp), intent(in) :: x, eta
      complex(dp), intent(out) :: f, g, df, dg
      complex(dp) :: fc(nl+1),gc(nl+1),fcp(nl+1),gcp(nl+1),sig(nl+1)
      complex(dp) :: xlmin
      integer :: mode1, kfn, idx

      kfn = 0
      xlmin = complex(0._dp, 0._dp)
      idx = nl + 1
      mode1 = 1

      call coulcc(x,eta,xlmin,nl+1, fc,gc,fcp,gcp, sig, mode1,kfn,ifail)
!     call coulcc(x,eta,zlmin,nl, fc,gc,fcp,gcp, sig, mode1,kfn,ifail)

      print *, 'fc = ', fc

      f = fc(idx)
      g = gc(idx)
      df = fcp(idx)
      dg = gcp(idx)

end subroutine

program main
    implicit none

      integer, parameter :: dp = selected_real_kind(15)
      integer :: ifail, nl
      complex(dp) :: x, eta
      complex(dp) :: f, g, df, dg

      x = complex(13.71356783919598_dp, 0._dp)
      eta = complex(0._dp, 0._dp)
      nl = 0

      ifail = 0

      call coulcc_wrapper(x,eta,nl,f,g,df,dg,ifail)

      print *, x, f, g, df, dg, ifail

end program main

!subroutine coulcc_wrapper(xr,xi,etar,etai,nl, fr,gr,dfr,dgr, fi,gi,dfi,dgi, ifail)
!      implicit none
!
!      integer, parameter :: dp = selected_real_kind(15)
!      integer, intent(out) :: ifail
!      integer, intent(in) :: nl
!      real(dp), intent(in) :: xr, etar
!      real(dp), intent(in) :: xi, etai
!      real(dp), intent(out) :: fr, gr, dfr, dgr
!      real(dp), intent(out) :: fi, gi, dfi, dgi
!      complex(dp) :: x, eta
!      complex(dp) :: fc(nl+1),gc(nl+1),fcp(nl+1),gcp(nl+1),sig(nl+1)
!      complex(dp) :: xlmin
!      integer :: mode1, kfn, idx
!
!      kfn = 0
!      xlmin = complex(0._dp, 0._dp)
!      idx = nl + 1
!      mode1 = 1
!      x = complex(xr, xi)
!      eta = complex(etar, etai)
!
!      call coulcc(x,eta,xlmin,nl+1, fc,gc,fcp,gcp, sig, mode1,kfn,ifail)
!!     call coulcc(x,eta,zlmin,nl, fc,gc,fcp,gcp, sig, mode1,kfn,ifail)
!
!      print *, 'fc = ', fc
!
!      fr = realpart(fc(idx))
!      gr = realpart(gc(idx))
!      dfr = realpart(fcp(idx))
!      dgr = realpart(gcp(idx))
!
!end subroutine
