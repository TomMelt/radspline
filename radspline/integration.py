import numpy as np


class GaussLegendre():
    """docstring for Gauss-Legendre integration class"""
    def __init__(self, order):

        self.order = order
        super(GaussLegendre, self).__init__()
        self.x, self.w = np.polynomial.legendre.leggauss(order)

    def integrate(self, a, b, bto_a, bto_b, func=None, deriv=0):

        x, w = self.x, self.w
        t = 0.5*(x + 1)*(b - a) + a

        if func is None:
            integral = sum(w * bto_a.eval(t)[:, 0] * bto_b.eval(t)[:, deriv]) * 0.5*(b - a)
        else:
            integral = sum(w * bto_a.eval(t)[:, 0] * func(t) * bto_b.eval(t)[:, deriv]) * 0.5*(b - a)

        return integral

    def converge_int(self, a, b, bto_a, bto_b, func=None, deriv=0, tol=1e-15):

        err = 10.*tol
        min_iter, max_iter = 1, 15

        int_new = 0.0
        int_old = self.integrate(a, b, bto_a, bto_b, func=func, deriv=deriv)

        while (err > tol):
            if min_iter > max_iter:
                msg = 'WARNING :: max_iteration reached.'
                msg += f'\nError estimate is: {err}'
                print(msg)
                return int_old

            segments = np.linspace(a, b, 2+min_iter)

            temp = []
            for c, d in list(zip(segments[:-1], segments[1:])):
                integral = self.integrate(c, d, bto_a, bto_b, func=func, deriv=deriv)
                temp.append(integral)

            int_new = sum(np.array(temp))

            err = abs(int_new-int_old)
            min_iter += 1
            int_old = int_new
#             print('iter={0: 4d}, int_new={1: 16.14f}, err={2: 10.4e}'.format(min_iter, int_new, err))

        return int_old


def bto_integrate(bto1, bto2, knots, deriv=0, func=None):

    if bto1.support[0] == min(bto1.support[0], bto2.support[0]):
        bto_a = bto1
        bto_b = bto2
    else:
        bto_a = bto2
        bto_b = bto1

    i = bto_a.num
    j = bto_b.num
    deg = bto_a.degree

    # if btos are more than "deg" knots apart they do not overlap
    if abs(i-j) > deg:
        return 0.

    # if they do overlap then calculate the integral in each segment
    else:
        gl_order = bto_a.degree + bto_b.degree + 1
        gl = GaussLegendre(gl_order)
        segment_integrals = []

        # start from the first knot of bto_b up to and including (+1) the last knot of bto_a
        for n in range(j, i+deg+1):
            a, b = knots[n], knots[n+1]
            if b > a:
                if func is None:
                    segment_integrals.append(
                            # must pass bto1 and bto2 so they are in the correct order
                            # instead of bto_a and bto_b.
                            gl.integrate(a, b, bto1, bto2, deriv=deriv)
                            )
                else:
                    segment_integrals.append(
                            # must pass bto1 and bto2 so they are in the correct order
                            # instead of bto_a and bto_b.
                            gl.converge_int(a, b, bto1, bto2, func=func, deriv=deriv)
                            )

        # the total integral will be the sum of each segment
        integral = sum(np.array(segment_integrals))

        return integral
