from .bspline import locate_knot
from .integration import bto_integrate
from scipy.linalg import eigh
from scipy.special import gamma, lpmv
import numpy as np
from . import special as sp


class Potential():
    """
    Depending on the initialisation this potential class can be used for 3
    types of potential:
    1. charge > 0., width = strength = 0 -- coulomb potential
    2. charge, width and strength > 0. -- coulomb + square well potential
    3. charge = 0., width and strength > 0. -- square well potential

    if l > 0 then the centrifugal barrier is calculated in addition
    """
    def __init__(self, strength, width, charge, model='', l=0):
        self.strength = strength
        self.width = width
        self.charge = charge
        self.l = l
        self._v_potential = np.vectorize(self._potential)
        self.model = model
        models = {
                'Ne': [ 8.069, 2.148,  -3.570, 1.986, 0.931, 0.602],
                'Ar': [16.039, 2.007, -25.543, 4.525, 0.961, 0.443],
                'Kr': [12.461, 1.451,  23.539, 5.644, 0.000, 0.000],
                'Xe': [51.356, 2.112, -99.927, 3.737, 1.644, 0.431],
                }
        if self.model != '':
            try:
                self.model_params = models[model]
            except KeyError:
                msg = f'Warning :: Potential model [{model}] not supported.'
                msg += '\nSupported model potentials are: Ne, Ar, Kr and Xe.'
                msg += '\nNo model potential will be used.'
                print(msg)

    def _potential(self, r):
        V = 0.
        Z = self.charge
        V0 = self.strength
        l = self.l
        # Square potential
        if V0 > 0.:
            if 0. <= r <= self.width:
                V = -V0
        # Coulombic potential
        if Z > 0.:
            V += -Z / r
        # model potential
        if self.model != '':
            a = self.model_params
            temp_V = a[0] * np.exp(-a[1] * r)
            temp_V += a[2] * r * np.exp(-a[3] * r)
            temp_V += a[4] * np.exp(-a[5] * r)
            temp_V = -temp_V / r
            V += temp_V
        # Centrifugal barrier
        if l > 0:
            V += l * (l + 1) / (2. * r**2)
        return V

    def __call__(self, r):
        return self._v_potential(r)


def solve_inner_region_hamiltonian(basis, l, pot_opts):

    dim = len(basis.btos)
    knots = basis.knots
    btos = basis.btos
    rmat_radius = basis.rmax

    T = np.zeros((dim, dim))
    V = np.zeros((dim, dim))
    S = np.zeros((dim, dim))
    L = np.zeros((dim, dim))

    width = pot_opts.width
    strength = pot_opts.strength
    charge = pot_opts.charge
    model = pot_opts.model
    potential = Potential(
            strength=strength,
            width=width,
            charge=charge,
            model=model,
            l=l,
            )

    for i in range(dim):
        for j in range(dim):
            T[i, j] = -0.5 * bto_integrate(btos[i], btos[j], knots, deriv=2)
            V[i, j] = bto_integrate(btos[i], btos[j], knots, func=potential)
            S[i, j] = bto_integrate(btos[i], btos[j], knots)
            L[i, j] = bloch_operator(btos[i], btos[j], knots, r=rmat_radius)

    H = T + V
    eigs, vecs = eigh(H + L, S)

    return eigs, vecs


def _rmatrix(k, amps, eigs, a):

    n = len(eigs)
    R = 0.
    E = 0.5 * k**2

    for i in range(n):
        R += amps[i] * amps[i] / (eigs[i] - E)

    R = R / (2. * a)

    return R


rmatrix = np.vectorize(_rmatrix, excluded=['amps', 'eigs', 'a'])


def boundary_amplitudes(vecs, btos, a):

    amps = vecs[-1, :] * btos[-1].eval([a])[:, 0]

    return amps


def wavefunction_inner(r, basis, eigenvectors, Ak_coeffs):
    n = len(basis.btos)
    bto_funcs = np.zeros((n, len(r)))
    rmat_funcs = np.zeros((n, len(r)))
    for i in range(n):
        bto_funcs[i] = basis.btos[i].eval(r)[:,0]

    rmat_funcs = np.dot(eigenvectors.T, bto_funcs)

    return np.dot(Ak_coeffs, rmat_funcs)


def Ak_coefficients(k, eigs, amps, delta, a, charge, l):

    n = len(eigs)
    Ak = np.zeros(n)
    E = 0.5 * k**2
    K = np.tan(delta)

    eta = -charge / k

    s, c, ds, dc = sp.coulomb_functions(x=a*k, eta=eta, l=l)

    for i in range(n):
        Ak[i] = 1. / (2. * (eigs[i] - E)) * amps[i] * k * (ds.real + K * dc.real)

    return Ak


def _phaseshift(k, R, l, a, charge):
    """obtain phaseshift for a scattering wavefunction

    Keyword Arguments
    :k: float
        momentum of scattering particle
    :R: float
        R-matrix
    :l: int
        angular momentum of scattering particle
    :a: float
        match solutions at r=a. (rmatrix boundary)

    :returns: float
        return the phaseshift compared to the free asymptotic wavefunction
    """

    eta = -charge / k

    s, c, ds, dc = sp.coulomb_functions(x=a*k, eta=eta, l=l)

    y = a * k * R * ds.real - s.real
    x = c.real - a * k * R * dc.real

    return np.arctan2(y, x)


phaseshift = np.vectorize(_phaseshift, excluded=['l', 'a', 'charge'])


@np.vectorize
def partial_xs(k, eigenphase, l):
    xs = 4 * np.pi * (2 * l + 1) * np.sin(eigenphase)**2
    xs = xs / (k * k)
    return xs


def fs(k, eigenphase, l, charge, theta):
    eta = -charge / k
    sigma = coulomb_phase(l=l, eta=eta)
    f = (2. * l + 1.) * np.exp(2.j * sigma) * np.exp(1.j * eigenphase) 
    f *= np.sin(eigenphase) * lpmv(0, l, np.cos(theta))
    f = f / k
    return f


def fc(k, charge, theta):
    eta = -charge / k
    s0 = coulomb_phase(l=0, eta=eta)
    f = -eta * np.exp(2.j * s0)
    f *= np.exp(-1.j * eta * np.log(np.sin(theta / 2.)**2))
    f = f / (2. * k * np.sin(theta / 2.)**2)
    return f


def _cumulative_differential_xs(k, eigenphases, lsum, charge, theta_degrees):
    theta = np.radians(theta_degrees)
    f1 = fc(k=k, charge=charge, theta=theta)
    f2 = 0.
    for l in range(lsum+1):
        f2 += fs(
                k=k,
                eigenphase=eigenphases[l],
                l=l,
                charge=charge,
                theta=theta,
                )
    f = f1 + f2
    return np.abs(f)**2

cumulative_differential_xs = np.vectorize(_cumulative_differential_xs, excluded=['eigenphases', 'lsum', 'charge'])


def coulomb_phase(l, eta):
    return np.angle(gamma(l + 1 + 1.j * eta))


def bloch_operator(bto1, bto2, knots, r):

    i, j = bto1.num, bto2.num
    degree = bto1.degree

    # if btos are more than "degree" knots apart they do not overlap
    if abs(i - j) > degree:
        return 0.

    # if they do overlap then calculate the result of the bloch operator
    m = locate_knot(r, knots)
    ans = 0.
    if i < m and i >= m - degree:
        if j < m and j >= m - degree:
            ans = 0.5 * bto1.eval(r)[0] * bto2.eval(r)[1]

    return ans
