from scipy.ndimage.filters import minimum_filter
from .special import hankel_functions, coulomb_functions
import numpy as np
import cmath as cm


def find_poles(grid, neighborhood_size=5):

    data_poles = minimum_filter(grid, neighborhood_size)
    poles = (grid[1:-1, 1:-1] == data_poles[1:-1, 1:-1])
    x, y = np.where(poles)

    return x, y


@np.vectorize
def siegert_root(R, k, l, r, charge):
    eta = charge / k
    # we need H+ functions hence pm = +1
    H, dH = hankel_functions(pm=+1, x=k*r, l=l, eta=eta)
    return np.abs(H - R * r * k * dH)**2





#@np.vectorize
#def smatrix(R, k, l, r, charge):
#
#    eta = -charge / k
#    a = r
##    s, ds = hankel_functions(pm=-1, x=a*k, eta=eta, l=l)
##    c, dc = hankel_functions(pm=+1, x=a*k, eta=eta, l=l)
#    s, c, ds, dc = coulomb_functions(x=a*k, eta=eta, l=l)
#
#    y = a * k * R * ds - s
#    x = c - a * k * R * dc
#
#    delta = cm.atan(y/x)
#
#    S = np.exp(2.j*delta)
#
#    return np.abs(S)**2
