subroutine coul90_wrapper(x,eta,nl, f,g,df,dg, ifail)
      implicit none

      integer, parameter :: dp = selected_real_kind(15)
      integer, intent(out) :: ifail
      integer, intent(in) :: nl
      real(dp), intent(in) :: x, eta
      real(dp), intent(out) :: f, g, df, dg
      real(dp) :: fc(nl+1),gc(nl+1),fcp(nl+1),gcp(nl+1)
      real(dp) :: xlmin
      integer :: kfn, idx

      kfn = 0
      xlmin = 0._dp
      idx = nl + 1

      call coul90(x,eta,xlmin,nl, fc,gc,fcp,gcp, kfn,ifail)

      f = fc(idx)
      g = gc(idx)
      df = fcp(idx)
      dg = gcp(idx)

end subroutine

program main
    implicit none

      integer, parameter :: dp = selected_real_kind(15)
      integer :: ifail, nl
      real(dp) :: x, eta
      real(dp) :: f, g, df, dg

      x = 1._dp
      eta = 0._dp
      nl = 0

      ifail = 0

      call coul90_wrapper(x,eta,nl,f,g,df,dg,ifail)

      print *, x, f, g, df, dg, ifail

end program main
