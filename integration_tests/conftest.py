def pytest_addoption(parser):
    parser.addoption("--plot", action="store", default="turn on/off plotting")

def pytest_generate_tests(metafunc):
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixtureplots".
    option_value = metafunc.config.option.plot
    if 'plot' in metafunc.fixturenames and option_value is not None:
        if option_value.lower() == 'true':
            option_value = True
        else:
            option_value = False
        metafunc.parametrize("plot", [option_value])
