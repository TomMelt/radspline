import pytest
from driver.driver import main, InnerRegion, OuterRegion
from plot import plot_compare
import numpy as np

testname = 'square1'
namelist = f'{testname}.nml'
main(namelist=namelist)


@pytest.mark.parametrize('field', ['eigenvalues', 'eigenvectors', 'amplitudes'])
def test_inner(allclose, field):
    new_inner = InnerRegion()
    new_inner.read_from_folder(folder=f'./output/{testname}')

    ref_inner = InnerRegion()
    ref_inner.read_from_folder(folder=f'./benchmark_output/{testname}')

    new_attr = new_inner.__getattribute__(field)
    ref_attr = ref_inner.__getattribute__(field)

    assert allclose(new_attr, ref_attr)


@pytest.mark.parametrize('field', ['eigenphases', 'partial_xs'])
def test_outer(allclose, field, plot):
    new_outer = OuterRegion()
    new_outer.read_from_folder(folder=f'./output/{testname}')

    ref_outer = OuterRegion()
    ref_outer.read_from_folder(folder=f'./benchmark_output/{testname}')

    new_attr = new_outer.__getattribute__(field)
    ref_attr = ref_outer.__getattribute__(field)

    if plot:
        new_data = np.genfromtxt(f'output/{testname}/{field}.dat')
        ref_data = np.genfromtxt(f'benchmark_output/{testname}/{field}.dat')
        plot_compare(ref_data, new_data)

    assert allclose(new_attr, ref_attr)
