import matplotlib.pyplot as plt


def plot_compare(ref, new):

    ref = ref.T
    new = new.T

    x1 = ref[0, :]
    y1 = ref[1:, :]

    x2 = new[0, :]
    y2 = new[1:, :]

    fig, axes = plt.subplots(2, 2, sharex=True, figsize=(16,4))
    axes = axes.flatten()

    for i in range(len(axes)):
        ax = axes[i]
        ax.plot(x2, y2[i], '-k', label='new data')
        ax.plot(x1, y1[i], '--r', label='ref data')

    axes[-1].legend(bbox_to_anchor=(1.04,1), loc="upper left", ncol=1)

    plt.show()
    return
