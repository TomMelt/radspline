from radspline.bspline import BsplineBasisSet
from radspline.settings import Options
from radspline import analytic, rmat
from dataclasses import asdict, dataclass
import numpy as np
import os


class InnerRegion():
    """docstring for InnerRegion"""
    def __init__(self):
        self.eigenvalues = np.ndarray
        self.eigenvectors = np.ndarray
        self.amplitudes = np.ndarray
        self.is_complete = False
        self.options = None

    def read_from_nml(self, namelist):
        opts = Options(namelist)
        debug(2, opts.raw_nml, 'namelist')
        debug(1, opts, 'options')
        self.options = opts

    def read_from_folder(self, folder):
        self.is_complete = True
        path = os.path.join(os.getcwd(), folder)
        fields = ['eigenvalues', 'eigenvectors', 'amplitudes']
        try:
            opts = Options(os.path.join(path, 'inputs.nml'))
            debug(2, opts.raw_nml, 'namelist')
            debug(1, opts, 'options')
            self.options = opts

            for field in fields:
                data = np.genfromtxt(
                        os.path.join(path, field+'.dat'),
                        dtype=np.float,
                        )
                if field == 'eigenvectors':
                    nx, ny = data.shape
                    l = opts.calculation.lmax + 1
                    data = data.reshape(nx//l, ny, l)
                data = data.T
                self.__setattr__(field, data)

        except FileNotFoundError as e:
            print(e)
            exit()

    def save(self):
        opts = self.options
        name = opts.calculation.name
        path = os.path.join(os.getcwd(), 'output/'+name)
        fields = ['eigenvalues', 'eigenvectors', 'amplitudes']
        try:
            os.makedirs(path)
        except FileExistsError:
            msg = f'output folder [{path}] already exists'
            obj = 'overwriting data...'
            debug(1, obj, msg)

        with open(os.path.join(path, 'inputs.nml'), 'w') as infile:
            infile.write(opts.raw_nml)

        for field in fields:
            data = self.__getattribute__(field)
            data = data.T # this make the text file easier to diff
            if field == 'eigenvectors':
                nx, ny, l = data.shape
                data = data.reshape(l*nx, ny)
            np.savetxt(os.path.join(path, field+'.dat'), data, fmt='%20.12e')

    def run(self):
        opts = self.options
        potential = rmat.Potential(**asdict(opts.potential))
        basis = BsplineBasisSet(**asdict(opts.bspline))

        n = len(basis.btos)
        lmax = opts.calculation.lmax
        rmat_radius = opts.bspline.rmax

        if opts.potential.model != '':
            msg = f'Model potential at rmatrix boundary: V({rmat_radius})'
            obj = potential(rmat_radius)+opts.potential.charge/rmat_radius
            debug(1, obj=obj, msg=msg)

        eigenvalues = np.zeros((lmax+1, n))
        eigenvectors = np.zeros((lmax+1, n, n))
        amplitudes = np.zeros((lmax+1, n))

        for l in range(lmax+1):
            eigs, vecs = rmat.solve_inner_region_hamiltonian(
                basis=basis,
                l=l,
                pot_opts=opts.potential,
            )
            eigenvalues[l] = eigs
            eigenvectors[l] = vecs
            amplitudes[l] = rmat.boundary_amplitudes(
                vecs=eigenvectors[l],
                btos=basis.btos,
                a=rmat_radius,
            )

        self.eigenvalues = eigenvalues
        self.eigenvectors = eigenvectors
        self.amplitudes = amplitudes
        self.is_complete = True


class OuterRegion():
    """docstring for OuterRegion"""
    def __init__(self, inner_results=None):
        self.inner_results = inner_results
        self.eigenphases = np.ndarray
        self.partial_xs = np.ndarray
        self.diff_xs_theta = None
        self.options = None
        self.k_grid = np.ndarray
        self.diff_grid_theta = None

    def read_from_folder(self, folder):
        path = os.path.join(os.getcwd(), folder)
        fields = ['partial_xs', 'eigenphases', 'diff_xs_theta']
        try:
            opts = Options(os.path.join(path, 'inputs.nml'))
            debug(2, opts.raw_nml, 'namelist')
            debug(1, opts, 'options')
            self.options = opts

            for field in fields:
                data = np.genfromtxt(
                        os.path.join(path, field+'.dat'),
                        dtype=np.float,
                        )
                if field in ['partial_xs', 'eigenphases']:
                    self.k_grid = data[:,0]
                    data = data[:,1:]
                if field == 'diff_xs_theta':
                    self.diff_grid_theta = data[:,0]
                    data = data[:,1:]
                    k_values = opts.calculation.diff_grid_theta[3:]
                    nk = len(k_values)
                    nx, ny_nk = data.shape
                    data = data.reshape(nx, ny_nk//nk, nk)
                data = data.T
                self.__setattr__(field, data)

        except OSError as e:
            msg = f'Warning: field [{field}] not found.'
            obj = 'RadSpline will continue.'
            debug(1, obj, msg)

    def save(self):
        opts = self.options
        name = opts.calculation.name
        path = os.path.join(os.getcwd(), 'output/'+name)
        fields = ['partial_xs', 'eigenphases', 'diff_xs_theta']
        try:
            os.makedirs(path)
        except FileExistsError:
            msg = f'output folder [{path}] already exists'
            obj = 'overwriting data...'
            debug(1, obj, msg)

        with open(os.path.join(path, 'inputs.nml'), 'w') as infile:
            infile.write(opts.raw_nml)

        for field in fields:
            data = self.__getattribute__(field)
            if data is None:
                continue
            data = data.T # this make the text file easier to diff
            if field in ['partial_xs', 'eigenphases']:
                k = self.k_grid
                k = k.reshape(len(k), 1)
                data = np.hstack((k, data))
            if field == 'diff_xs_theta':
                theta = self.diff_grid_theta
                theta = theta.reshape(len(theta), 1)
                data = self.diff_xs_theta
                nx, ny, nk = data.shape
                data = data.reshape((nx, ny*nk))
                data = np.hstack((theta, data))

            np.savetxt(
                    os.path.join(path, field+'.dat'),
                    data,
                    fmt='%20.12e',
                    )


    def run(self):
        inner_results = self.inner_results
        self.options = self.inner_results.options
        opts = self.options
        calc_opts = opts.calculation

        lmax = calc_opts.lmax
        kmin, kmax, ksteps = calc_opts.k_grid
        k = np.linspace(kmin, kmax, ksteps)

        eigenphases = np.zeros((lmax+1, len(k)))
        partial_xs = np.zeros((lmax+1, len(k)))

        amplitudes = inner_results.amplitudes
        eigenvalues = inner_results.eigenvalues
        eigenvectors = inner_results.eigenvectors

        a = opts.bspline.rmax

        for l in range(lmax + 1):

            R = rmat.rmatrix(k=k, amps=amplitudes[l], eigs=eigenvalues[l], a=a)

            eigenphase = rmat.phaseshift(
                    k=k,
                    R=R,
                    l=l,
                    a=a,
                    charge=opts.potential.charge,
                    )
            eigenphase = np.mod(eigenphase, np.pi)
            partial_x = rmat.partial_xs(k=k, eigenphase=eigenphase, l=l)

            eigenphases[l] = eigenphase
            partial_xs[l] = partial_x

        self.k_grid = k
        self.eigenphases = eigenphases
        self.partial_xs = partial_xs

        if calc_opts.diff_grid_theta != []:

            temp = calc_opts.diff_grid_theta
            theta_min = float(temp.pop(0))
            theta_max = float(temp.pop(0))
            theta_steps = int(temp.pop(0))
            theta = np.linspace(theta_min, theta_max, theta_steps)
            self.diff_grid_theta = theta

            k_values = np.array(temp, dtype=np.float)

            diff_xs = np.zeros((len(k_values), lmax+1, theta_steps))

            eigenphases = np.zeros((lmax+1, len(k_values)))

            for l in range(lmax + 1):
                R = rmat.rmatrix(
                        k=k_values,
                        amps=inner_results.amplitudes[l],
                        eigs=inner_results.eigenvalues[l],
                        a=a,
                        )

                eigenphases[l] = rmat.phaseshift(
                        k=k_values,
                        R=R,
                        l=l,
                        a=a,
                        charge=opts.potential.charge,
                        )

            for i, k in enumerate(k_values):
                for l in range(lmax + 1):
                    diff_xs[i, l, :] = rmat.cumulative_differential_xs(
                            k=k,
                            eigenphases=eigenphases[:l+1, i],
                            lsum=l,
                            charge=opts.potential.charge,
                            theta_degrees=theta,
                            )

            self.diff_xs_theta = diff_xs.T
            del(diff_xs)


def debug(level, obj, msg):
    opts = Options()
    debug_level = opts.calculation.debug
    if debug_level >= level:
        print('---------',msg,'---------')
        print(obj)
        print('--------------------------')
    return


def main(namelist=None, folder=None):

    if namelist:
        inner = InnerRegion()
        inner.read_from_nml(namelist=namelist)
        inner.run()
        opts = inner.options
        if opts.calculation.save_inner:
            inner.save()
        outer = OuterRegion(inner_results=inner)
        outer.run()
        if opts.calculation.save_outer:
            outer.save()
    elif folder:
        inner = InnerRegion()
        inner.read_from_folder(folder=folder)
        outer = OuterRegion(inner_results=inner)
        outer.read_from_folder(folder=folder)
    else:
        msg = 'Error: You must supply a folder or namelist.'
        raise FileNotFoundError(msg)

    return

if __name__ == "__main__":
    import sys
    import time

    start_time = time.time()
    inputs = sys.argv[1]
    if inputs.lower()[-4:] == '.nml':
        main(namelist=inputs)
    else:
        main(folder=inputs)
    end_time = time.time()
    print(f'Time elapsed: {end_time-start_time:.3f} [s]')
    print('RadSpline has completed successfully.')
