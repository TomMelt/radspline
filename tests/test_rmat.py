import pytest
import radspline.rmat as rmat
import numpy

@pytest.mark.parametrize(
        "strength, width, charge, l, r, expects",
        [
            # test square well
            (10., 2., 0., 0, 0., -10.),
            (10., 2., 0., 0, 2., -10.),
            (10., 2., 0., 0, 3., 0.),
            # test centrifugal
            (0., 0., 0., 1, 1., 1.0),
            (0., 0., 0., 0, 2., 0.0),
            (0., 0., 0., 2, 3., 1./3.),
            # test Coulomb
            (0., 0., 1.5, 0, 1., -1.5),
            (0., 0., 1.5, 0, 3., -0.5),
            ]
        )
def test_Potential_values(strength, width, charge, l, r, expects):
    potential_opts = {
            'strength': strength,
            'width': width,
            'charge': charge,
            'l': l,
            }

    potential = rmat.Potential(**potential_opts)

    assert potential(r) == pytest.approx(expects)

@pytest.mark.parametrize(
        "charge, l", [(2., 0), (0., 1), (3., 2)]
        )
def test_Potential_zero_division(charge, l):
    potential_opts = {
            'strength': 0.,
            'width': 0.,
            'charge': charge,
            'l': l,
            }
    potential = rmat.Potential(**potential_opts)

    with pytest.warns(RuntimeWarning):
        with pytest.raises(ZeroDivisionError):
            potential(r=0.)
