Cython>=0.29.22
geomdl>=5.3.0
scipy>=1.2.1
f90nml>=1.2
numpy>=1.18.1
pytest-allclose>=1.0.0
f2py
