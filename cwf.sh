#!/usr/bin/bash

cd cwf_cpp/

python setup.py build_ext --inplace

cd ..

cp -vf cwf_cpp/cwf_wrapper*.so radspline/
