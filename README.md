RadSpline
=========

RadSpline is a python module that solves the 1D time independent Schrodinger
equation using the R-matrix method. R-matrix basis functions are constructed
from B-splines.

![RadsplineImage](diagram.png)

Requirements
============

The following packages are required but will be install automatically by pip
* `cython>=0.29.22` - Cython - Used to compile cwf c++ code into a python module
* `geomdl>=5.3.0` - NURBS B-spline library
* `f90nml>=1.2` - Fortran 90 namelist parser
* `pytest-allclose>=1.0.0` - For the test suite
* `scipy>=1.2.1`
* `numpy>=1.18.1`

The following package is required and must be installed manually
* `f2py` - Compile Fortran source into python module (installed separately)

Installation
============

1. Clone the repo

    `git clone git@gitlab.com:TomMelt/radspline.git`

    `cd radspline`

1. Compile the Fortran source into a python module using f2py

    `bash cwf.sh`

1. Install

    `pip install -U -e .`

1. Run tests

    `cd tests/; pytest -v .; cd ..`

    `cd integration_tests/; pytest -v .; cd ..`

    *(optional)* run test with plots

    `cd integration_tests/; pytest -v --plot True .; cd ..`

To Do
=====

* expand integration tests for model potentials
* Finish unit tests
* implement `COULCC.f90`
* adapt Siegert state analysis for Coulombic potentials
* add keyword to print wavefunction
* split rmat.py into inner.py and outer.py
* b-spline end points are currently calculated in a crude fashion (x-1e-8). In theory I can used nth order interpolation where n is the degree of the B-spline.

Nice to have
============

* add pretty print for objects InnerRegion and OuterRegion
    - for nd arrays print shape
    - for everything else print value if not None?
